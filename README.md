
# first you have to install serverless
npm install -g serverless

# create your own folder
mkdir new-shiny-project
cd new-shiny-project

# you have to sing up on serverless website
serverless login

# create your own project (java based)
serverless create -t "aws-java-maven"

# Open the project in your beloved IDE
add this to pom.xml to dependencies section:
<dependency>
      <groupId>com.amazonaws</groupId>
      <artifactId>aws-java-sdk-dynamodb</artifactId>
      <version>1.11.648</version>
</dependency>



# add this to the serverless.yml
resources: # CloudFormation template syntax from here on.
  Resources:
    usersTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: usersTable
        AttributeDefinitions:
          - AttributeName: email
            AttributeType: S
        KeySchema:
          - AttributeName: email
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1

plugins:
  - serverless-dynamodb-local

custom:
  dynamodb:
    # If you only want to use DynamoDB Local in some stages, declare them here
    stages:
      - local
    start:
      port: 8000
      inMemory: true
      heapInitial: 200m
      heapMax: 1g
      migrate: true
      seed: true
      convertEmptyValues: true



# copy paste Handler class from this repo and replace with your Handler class which is in src/main/java/com/serverless also replace ApiGatewayResponse class
# same way copy paste ApiGatewayResponse class
# go to pom.xml and downgrade aws-lambda-java-log4j2 library to 1.0.0 from 1.1.0  ( it's needed cause there is a bug in this lib)

# install local dynamodb in project folder
npm install --save serverless-dynamodb-local

# install local dynamodb
sls dynamodb install

# run dynamo db
sls dynamodb start


# run mvn clean package

# run it in local mode
serverless invoke local --function hello -d '{"email":"bar"}'

you will see body = 1 in output

then run 
serverless invoke local --function hello -d '{"email":"baz"}'

and see that it's being incremented


# to deploy it on aws  ( in Handler class you have to change region and dynamo endpoint, of course for production I will move it to properties but I didn't have time to do that today)
serverless deploy -s dev





