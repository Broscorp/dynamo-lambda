package com.serverless;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Handler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

  private static final Logger LOG = LogManager.getLogger(Handler.class);
  private AmazonDynamoDB client;

  private String DYNAMODB_TABLE_NAME = "usersTable";

  public Handler() {
    createDynamoClient();
  }

  @Override
  public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
    LOG.info("received: {}", input);

    persistData(input);

    final ScanRequest scanRequest = new ScanRequest()
        .withTableName(DYNAMODB_TABLE_NAME);
    final ScanResult result = client.scan(scanRequest);

    return ApiGatewayResponse.builder()
        .setObjectBody(String.valueOf(result.getScannedCount()))
        .setStatusCode(200).build();
  }


  private void persistData(Map<String, Object> input)
      throws ConditionalCheckFailedException {
    try {
      HashMap<String, AttributeValue> item_values = new HashMap<>();

      item_values.put("email", new AttributeValue(input.get("email").toString()));
      client.putItem(DYNAMODB_TABLE_NAME, item_values);
    } catch (ResourceNotFoundException e) {
      System.err.format("Error: The table \"%s\" can't be found.\n", DYNAMODB_TABLE_NAME);
      System.err.println("Be sure that it exists and that you've typed its name correctly!");
      System.exit(1);
    } catch (AmazonServiceException e) {
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }

  private void createDynamoClient() {
    client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
        new AwsClientBuilder.EndpointConfiguration(System.getenv("DYNAMODB_URL"),
            System.getenv("DYNAMODB_REGION")))
        .build();
    LOG.info("Created DynamoDB client");
  }
}
